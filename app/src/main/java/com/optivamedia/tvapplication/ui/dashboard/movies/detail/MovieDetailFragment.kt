package com.optivamedia.tvapplication.ui.dashboard.movies.detail

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.optivamedia.tvapplication.R
import com.optivamedia.tvapplication.databinding.FragmentMovieDetailBinding
import com.optivamedia.tvapplication.domain.Movie
import com.optivamedia.tvapplication.ui.dashboard.DashboardActivity
import com.optivamedia.tvapplication.ui.dashboard.DashboardViewModel
import com.optivamedia.tvapplication.ui.dashboard.movies.MoviesAdapter
import com.optivamedia.tvapplication.ui.dashboard.movies.MoviesViewModel
import timber.log.Timber
import javax.inject.Inject


class MovieDetailFragment : Fragment(){
    @Inject
    lateinit var viewModelFactory : ViewModelProvider.Factory
    lateinit var binding : FragmentMovieDetailBinding
    private val dashboardViewModel : DashboardViewModel by activityViewModels{ viewModelFactory }
    private val viewModel : MoviesViewModel by viewModels { viewModelFactory }
    private var movie : Movie? = null
    private var isFavorite : Boolean = false
    private var adapter : MoviesAdapter? = null

    companion object {
        private const val ARG_MOVIE = "MOVIE"
        fun newInstance(movie : Movie?) =
                MovieDetailFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(ARG_MOVIE, movie)
                    }
                }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity() as DashboardActivity).dashboardComponent.inject(this)

        arguments?.let {
            movie = it.getSerializable(ARG_MOVIE) as? Movie?
            isFavorite = movie?.favorite ?: false
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_movie_detail, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.isFavorite = isFavorite
        setOnClickListener()
        viewModel.model.observe(this, Observer(::updateUi) )

        viewModel.getMovieDetail(movie?.externalId)

    }



    private fun updateUi(uiModel : MoviesViewModel.UiModel){
        when (uiModel){
            is MoviesViewModel.UiModel.Loading -> {
                dashboardViewModel.showLoading()
            }

            is MoviesViewModel.UiModel.FailureMovieDetail ->{
                dashboardViewModel.hideLoading()
                Toast.makeText(context, getString(R.string.err_movie_detail), Toast.LENGTH_SHORT).show()
            }

            is MoviesViewModel.UiModel.SuccessMovieDetail -> {
                dashboardViewModel.hideLoading()

                movie = uiModel.movie
                movie?.favorite = isFavorite
                binding.movie = uiModel.movie

                // now get movie recommendations
                viewModel.getMovieRecommendations(movie?.externalId)
            }


            is MoviesViewModel.UiModel.FailureSaveMovie -> {
                dashboardViewModel.hideLoading()
                Toast.makeText(context, getString(R.string.err_add_favorite), Toast.LENGTH_SHORT).show()
            }

            // Movie saved
            is MoviesViewModel.UiModel.SuccessSaveMovie -> {
                dashboardViewModel.hideLoading()
                binding.isFavorite = isFavorite // updating binding isFavorite

                // show message
                if (uiModel.movie?.favorite == true)
                    Toast.makeText(context, getString(R.string.msg_added_favorite), Toast.LENGTH_SHORT).show()
                else
                    Toast.makeText(context, getString(R.string.msg_removed_favorite), Toast.LENGTH_SHORT).show()
            }

            is MoviesViewModel.UiModel.FailureRecommendations -> {
                dashboardViewModel.hideLoading()
                Toast.makeText(context, getString(R.string.err_movie_recommendations), Toast.LENGTH_SHORT).show()
                binding.tvNoRecommendations.visibility = View.VISIBLE
            }

            is MoviesViewModel.UiModel.SuccessRecommendations -> {
                dashboardViewModel.hideLoading()
                if (uiModel.movies.isNullOrEmpty()){
                    binding.tvNoRecommendations.visibility = View.VISIBLE
                } else
                    setupRecyclerView(uiModel.movies)
            }

        }
    }

    private fun setupRecyclerView(movies : List<Movie>?){
        adapter = MoviesAdapter(movies, true, { position, movie ->
            // favorite Click listener
            viewModel.saveMovie(position, movie)
        }, {
            // item click listener
            dashboardViewModel.navigateToMovieDetail(it)
        })
        binding.rvRecommendations.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.rvRecommendations.adapter  = adapter
    }


    private fun setOnClickListener(){
        binding.ivFavorite.setOnClickListener{
            isFavorite = !(movie?.favorite?:false)
            movie?.favorite = isFavorite
            viewModel.saveMovie(null, movie)
        }
    }


    override fun onDestroy() {
        dashboardViewModel.hideLoading()
        super.onDestroy()
    }


}