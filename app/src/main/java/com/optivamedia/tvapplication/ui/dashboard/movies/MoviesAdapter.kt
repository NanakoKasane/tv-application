package com.optivamedia.tvapplication.ui.dashboard.movies

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.optivamedia.tvapplication.R
import com.optivamedia.tvapplication.databinding.ItemMovieBinding
import com.optivamedia.tvapplication.domain.Movie

class MoviesAdapter(
        private val items: List<Movie>?,
        private val isRecommendationList : Boolean = false,
        private val favoriteClickListener : (Int?, Movie?) -> Unit,
        private val itemClickListener : (Movie?) -> Unit
) : RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder>(), Filterable {
    var moviesFilterList : List<Movie>? = null

    init {
        moviesFilterList = items as ArrayList<Movie>
    }


    override fun getItemCount(): Int {
        return moviesFilterList?.size ?:0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder =
            MoviesViewHolder(
                    DataBindingUtil.inflate(
                            LayoutInflater.from(parent.context),
                            R.layout.item_movie,
                            parent,
                            false
                    )
            )

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        holder.bind(moviesFilterList?.get(position), position)
    }

    // FILTER by name
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    moviesFilterList = items
                } else {
                    val resultList = ArrayList<Movie>()
                    items?.forEach {
                        if (it.name?.toLowerCase()?.contains(constraint.toString().toLowerCase()) == true ) {
                            resultList.add(it)
                        }
                    }
                    moviesFilterList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = moviesFilterList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                moviesFilterList = results?.values as? ArrayList<Movie>
                notifyDataSetChanged()
            }
        }
    }

    fun updateList(movieList : List<Movie>?){
        moviesFilterList = movieList
        notifyDataSetChanged()
    }

    // VIEW HOLDER
    inner class MoviesViewHolder(val binding: ItemMovieBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(movie : Movie?, position : Int) {
            binding.movie = movie
            if (isRecommendationList){
                binding.ivFavorite.visibility = View.GONE
            }

            binding.cvContainer.setOnClickListener {
                itemClickListener(movie)
            }
            binding.ivFavorite.setOnClickListener {
                if (movie != null) {
                    movie.favorite = !movie.favorite
                    favoriteClickListener(position, movie)
                }
            }

        }
    }
}

