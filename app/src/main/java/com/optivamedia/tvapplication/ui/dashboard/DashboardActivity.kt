package com.optivamedia.tvapplication.ui.dashboard

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.optivamedia.tvapplication.App
import com.optivamedia.tvapplication.R
import com.optivamedia.tvapplication.databinding.ActivityDashboardBinding
import com.optivamedia.tvapplication.domain.Movie
import com.optivamedia.tvapplication.ui.dashboard.di.DashboardComponent
import com.optivamedia.tvapplication.ui.dashboard.movies.MoviesFragment
import com.optivamedia.tvapplication.ui.dashboard.movies.detail.MovieDetailFragment
import javax.inject.Inject

class DashboardActivity : AppCompatActivity(),
        BottomNavigationView.OnNavigationItemSelectedListener {
    lateinit var dashboardComponent: DashboardComponent
    @Inject
    lateinit var viewmodelFactory: ViewModelProvider.Factory
    private val viewModel: DashboardViewModel by viewModels { viewmodelFactory }
    private lateinit var binding: ActivityDashboardBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        dashboardComponent = (application as App).appComponent.dashboardComponent().create()
        dashboardComponent.inject(this)

        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard)
        binding.viewmodel = viewModel
        binding.bnvDashboard.setOnNavigationItemSelectedListener(this)
        viewModel.model.observe(this,  Observer(::updateUi) )

        binding.bnvDashboard.selectedItemId = R.id.navigation_movies

    }


    private fun updateUi(uiModel : DashboardViewModel.UiModel) {
        viewModel.hideLoading()
        when (uiModel) {
            is DashboardViewModel.UiModel.NavigateMovieDetail -> {
                loadMovieDetailView(uiModel.movie)
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        clearStack()
        viewModel.hideLoading()

        when (item.itemId) {
            R.id.navigation_movies -> {
                loadMoviesView()
            }
            R.id.navigation_favorites -> {
                loadMoviesView(true)
            }
        }
        return true
    }

    private fun loadMoviesView(isFavorite : Boolean = false ){
        supportFragmentManager.beginTransaction()
                .replace(
                        R.id.dashboard_content,
                        MoviesFragment.newInstance(isFavorite),
                        MoviesFragment::javaClass.name
                )
                .commit()
    }

    private fun loadMovieDetailView(movie : Movie?){
        supportFragmentManager.beginTransaction()
                .replace(
                        R.id.dashboard_content,
                        MovieDetailFragment.newInstance(movie),
                        MovieDetailFragment::javaClass.name
                )
                .addToBackStack(null)
                .commit()
    }



    private fun clearStack(){
        supportFragmentManager.popBackStack(null,
                FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }


}