package com.optivamedia.tvapplication.ui.dashboard.di

import androidx.lifecycle.ViewModel
import com.optivamedia.tvapplication.di.ViewModelKey
import com.optivamedia.tvapplication.ui.dashboard.DashboardViewModel
import com.optivamedia.tvapplication.ui.dashboard.movies.MoviesViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class DashboardModule {
    @Binds
    @IntoMap
    @ViewModelKey(DashboardViewModel::class)
    abstract fun bindViewModel(viewModel: DashboardViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MoviesViewModel::class)
    abstract fun bindMoviesViewModel(viewModel: MoviesViewModel): ViewModel
}