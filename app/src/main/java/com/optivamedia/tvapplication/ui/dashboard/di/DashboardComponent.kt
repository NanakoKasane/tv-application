package com.optivamedia.tvapplication.ui.dashboard.di

import com.optivamedia.tvapplication.di.ActivityScope
import com.optivamedia.tvapplication.ui.dashboard.DashboardActivity
import com.optivamedia.tvapplication.ui.dashboard.movies.MoviesFragment
import com.optivamedia.tvapplication.ui.dashboard.movies.detail.MovieDetailFragment
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [DashboardModule::class])
interface DashboardComponent {
    @Subcomponent.Factory
    interface Factory {
        fun create(): DashboardComponent
    }

    fun inject(activity: DashboardActivity)
    fun inject(fragment: MoviesFragment)
    fun inject(fragment: MovieDetailFragment)

}