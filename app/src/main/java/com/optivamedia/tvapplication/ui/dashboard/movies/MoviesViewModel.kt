package com.optivamedia.tvapplication.ui.dashboard.movies

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.optivamedia.tvapplication.data.exception.Failure
import com.optivamedia.tvapplication.data.functional.fold
import com.optivamedia.tvapplication.data.repository.MoviesRepository
import com.optivamedia.tvapplication.domain.Movie
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject


class MoviesViewModel @Inject constructor(private val moviesRepository: MoviesRepository) : ViewModel(){
    private val _model = MutableLiveData<UiModel>()
    val model: LiveData<UiModel> = _model

    fun getMovies(){
        _model.value = UiModel.Loading

        GlobalScope.launch {
            val result = moviesRepository.getMovies()
            result.fold({
                _model.postValue(UiModel.FailureMoviesList)
            }, {
                _model.postValue(UiModel.SuccessMoviesList(it))
            })
        }

    }

    fun saveMovie(position : Int?, movie : Movie?){
//        _model.value = UiModel.Loading

        GlobalScope.launch {
            val result = moviesRepository.saveMovie(movie)
            result.fold({
                _model.postValue(UiModel.FailureSaveMovie)
            },{
                // this long is the id inserted from database
                _model.postValue(UiModel.SuccessSaveMovie(position, movie))
            })
        }

    }

    fun getFavoritesMovies(){
        _model.value = UiModel.Loading

        GlobalScope.launch {
            val result = moviesRepository.getFavoritesMovies()
            result.fold({
                _model.postValue(UiModel.FailureFavoritesMoviesList)
            },{
                _model.postValue(UiModel.SuccessFavoritesMoviesList(it))
            })
        }
    }



    fun getMovieDetail(externalId : String?){
        _model.value = UiModel.Loading

        GlobalScope.launch {
            val result = moviesRepository.getMovieDetail(externalId)
            result.fold({
                _model.postValue(UiModel.FailureMovieDetail)
            }, {
                _model.postValue(UiModel.SuccessMovieDetail(it))
            })
        }
    }


    fun getMovieRecommendations(externalId : String?){
        _model.value = UiModel.Loading

        GlobalScope.launch {
            val result = moviesRepository.getMovieRecommendations(externalId)
            result.fold({
                _model.postValue(UiModel.FailureRecommendations)
            }, {
                _model.postValue(UiModel.SuccessRecommendations(it))
            })
        }
    }


    sealed class UiModel {
        object Loading : UiModel()
        object FailureMoviesList : UiModel()
        data class SuccessMoviesList(val movies : List<Movie>?) : UiModel()
        object FailureSaveMovie : UiModel()
        data class SuccessSaveMovie(val position : Int?, val movie: Movie?) : UiModel()
        object FailureFavoritesMoviesList : UiModel()
        data class SuccessFavoritesMoviesList(val movies : List<Movie>?) : UiModel()
        object FailureMovieDetail : UiModel()
        data class SuccessMovieDetail(val movie : Movie) : UiModel()
        object FailureRecommendations : UiModel()
        data class SuccessRecommendations(val movies : List<Movie>?) : UiModel()

    }


}