package com.optivamedia.tvapplication.ui.dashboard.movies

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.optivamedia.tvapplication.R
import com.optivamedia.tvapplication.data.extension.hideKeyboard
import com.optivamedia.tvapplication.databinding.FragmentMoviesBinding
import com.optivamedia.tvapplication.domain.Movie
import com.optivamedia.tvapplication.ui.dashboard.DashboardActivity
import com.optivamedia.tvapplication.ui.dashboard.DashboardViewModel
import javax.inject.Inject


class MoviesFragment : Fragment(){
    @Inject
    lateinit var viewModelFactory : ViewModelProvider.Factory
    lateinit var binding : FragmentMoviesBinding
    private val dashboardViewModel : DashboardViewModel by activityViewModels{ viewModelFactory }
    private val viewModel : MoviesViewModel by viewModels { viewModelFactory }
    private var adapter : MoviesAdapter? = null
    private var isFavorite : Boolean = false


    companion object {
        private const val ARG_IS_FAVORITE = "IS_FAVORITE"
        fun newInstance(isFavorite : Boolean = false) =
                MoviesFragment().apply {
                    arguments = Bundle().apply {
                        putBoolean(ARG_IS_FAVORITE, isFavorite)
                    }
                }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity() as DashboardActivity).dashboardComponent.inject(this)

        arguments?.let {
            isFavorite = it.getBoolean(ARG_IS_FAVORITE)
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_movies, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOnClickListener()
        setOnQuerySearchListener()

        viewModel.model.observe(this, Observer(::updateUi) )
        setupUi()
    }

    private fun setupUi(){
        if (!isFavorite) { // If it isn't favorite -> movies common title and icon
            binding.tvMoviesHeader.text = getString(R.string.it_movies)
            binding.ivIcon.setImageResource(R.drawable.ic_movies)
            binding.tvNoMovies.text = getString(R.string.tv_no_movies)

            viewModel.getMovies()
        }
        else { // If it is favorite -> favorite title and icon
            binding.tvMoviesHeader.text = getString(R.string.it_favorites)
            binding.ivIcon.setImageResource(R.drawable.ic_favorites)
            binding.tvNoMovies.text = getString(R.string.tv_no_favorites)

            // favorites movie request
            viewModel.getFavoritesMovies()
        }
    }

    private fun updateUi(uiModel : MoviesViewModel.UiModel){
        when (uiModel){
            is MoviesViewModel.UiModel.Loading -> {
                dashboardViewModel.showLoading()
            }

            is MoviesViewModel.UiModel.FailureMoviesList -> {
                dashboardViewModel.hideLoading()
                Toast.makeText(context, getString(R.string.err_obtain_movies), Toast.LENGTH_SHORT).show()
                binding.tvNoMovies.visibility = View.VISIBLE
            }

            is MoviesViewModel.UiModel.FailureSaveMovie -> {
                dashboardViewModel.hideLoading()
                Toast.makeText(context, getString(R.string.err_add_favorite), Toast.LENGTH_SHORT).show()
            }

            is MoviesViewModel.UiModel.FailureFavoritesMoviesList ->{
                dashboardViewModel.hideLoading()
                Toast.makeText(context, getString(R.string.err_obtain_favorites_movies), Toast.LENGTH_SHORT).show()
                binding.tvNoMovies.visibility = View.VISIBLE
            }

            // Movie list obtained
            is MoviesViewModel.UiModel.SuccessMoviesList -> {
                dashboardViewModel.hideLoading()
                binding.tvNoMovies.visibility = View.INVISIBLE

                setupRecyclerView(uiModel.movies)
            }

            // Movie saved
            is MoviesViewModel.UiModel.SuccessSaveMovie -> {
                dashboardViewModel.hideLoading()
                // if is favorite list, update all list
                if (isFavorite ) {
                    viewModel.getFavoritesMovies()
                }
                // in case is not favorite list, update only icon favorite or no favorite (depend on the "favorite" variable)
                else if (uiModel.position != null )
                    adapter?.notifyItemChanged(uiModel.position)

                // show message
                if (uiModel.movie?.favorite == true)
                    Toast.makeText(context, getString(R.string.msg_added_favorite), Toast.LENGTH_SHORT).show()
                else
                    Toast.makeText(context, getString(R.string.msg_removed_favorite), Toast.LENGTH_SHORT).show()
            }

            // Favorites movie list obtained
            is MoviesViewModel.UiModel.SuccessFavoritesMoviesList ->{
                dashboardViewModel.hideLoading()
                if (uiModel.movies.isNullOrEmpty())
                    binding.tvNoMovies.visibility = View.VISIBLE
                else
                    binding.tvNoMovies.visibility = View.INVISIBLE

                setupRecyclerView(uiModel.movies)
            }

        }
    }

    private fun setupRecyclerView(movies : List<Movie>?){
        adapter = MoviesAdapter(movies, false, { position, movie ->
            // favorite Click listener
            viewModel.saveMovie(position, movie)
        }, {
            // item click listener
            dashboardViewModel.navigateToMovieDetail(it)
        })
        binding.rvMovies.layoutManager = GridLayoutManager(activity, 2) // 2 rows
        binding.rvMovies.adapter  = adapter
    }


    private fun setOnClickListener(){

    }

    private fun setOnQuerySearchListener(){
        binding.searchview.setOnQueryTextListener(object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter?.filter?.filter(newText)
                return false
            }
        })

    }

    override fun onDestroy() {
        dashboardViewModel.hideLoading()
        super.onDestroy()
    }


}