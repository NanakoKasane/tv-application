package com.optivamedia.tvapplication.ui.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.optivamedia.tvapplication.domain.Movie
import com.optivamedia.tvapplication.ui.common.HasLoading
import com.optivamedia.tvapplication.ui.common.HasLoadingImpl
import javax.inject.Inject

class DashboardViewModel @Inject constructor() : ViewModel(), HasLoading by HasLoadingImpl() {
    private val _model = MutableLiveData<UiModel>()
    val model: LiveData<UiModel> = _model

    fun navigateToMovieDetail(movie: Movie?){
        _model.value = UiModel.NavigateMovieDetail(movie)
    }


    sealed class UiModel {
        data class NavigateMovieDetail(val movie : Movie?) : UiModel()
    }
}