package com.optivamedia.tvapplication

import android.app.Application
import com.optivamedia.tvapplication.data.extension.configure
import com.optivamedia.tvapplication.di.AppComponent
import com.optivamedia.tvapplication.di.DaggerAppComponent

open class App : Application() {
    val appComponent: AppComponent by lazy {
        DaggerAppComponent.factory().create(applicationContext)
    }

    override fun onCreate() {
        super.onCreate()
        configure()
    }
}