package com.optivamedia.tvapplication.di

import com.optivamedia.tvapplication.ui.dashboard.di.DashboardComponent
import dagger.Module

@Module(subcomponents = [DashboardComponent::class, DashboardComponent::class])
class AppSubcomponents