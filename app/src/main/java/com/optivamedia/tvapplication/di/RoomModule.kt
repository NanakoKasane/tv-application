package com.optivamedia.tvapplication.di

import android.content.Context
import com.optivamedia.tvapplication.data.room.TvApplicationDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule{
    @Provides
    @Singleton
    fun provideTvApplicationDatabase(context : Context) : TvApplicationDatabase {
        return TvApplicationDatabase.build(context)
    }
}