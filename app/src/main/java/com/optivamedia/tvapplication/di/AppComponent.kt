package com.optivamedia.tvapplication.di

import android.content.Context
import com.optivamedia.tvapplication.ui.dashboard.di.DashboardComponent
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
        modules = [ViewModelBuilderModule::class, AppSubcomponents::class, StorageModule::class, RoomModule::class, NetworkModule::class, EnvironmentModule::class])
interface AppComponent {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): AppComponent
    }

    fun dashboardComponent(): DashboardComponent.Factory

}