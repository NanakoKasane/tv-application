package com.optivamedia.tvapplication.di

import javax.inject.Qualifier


@MustBeDocumented
@Retention(value = AnnotationRetention.RUNTIME)
@Qualifier


annotation class OkHttpNetworkInterceptors