package com.optivamedia.tvapplication.di

import com.optivamedia.tvapplication.data.room.TvApplicationRoomDataSource
import com.optivamedia.tvapplication.data.sharedpreferences.SharedPreferencesDataSource
import com.optivamedia.tvapplication.data.source.PreferencesDataSource
import com.optivamedia.tvapplication.data.source.RoomDataSource
import dagger.Binds
import dagger.Module

@Module
abstract class StorageModule {
    @Binds
    abstract fun provideSharedPreferences(storage: SharedPreferencesDataSource): PreferencesDataSource

    @Binds
    abstract fun provideRoom(room : TvApplicationRoomDataSource) : RoomDataSource
}