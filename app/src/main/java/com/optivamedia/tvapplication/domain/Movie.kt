package com.optivamedia.tvapplication.domain

import java.io.Serializable

data class Movie(
        val id : String?,
        val year : Int?,
        val name : String?,
        val image : String?,
        val externalId : String?,
        val duration : String?,
        val type : String?,
        val keywordswords : String?,
        val description : String?,
        var favorite : Boolean = false
) : Serializable