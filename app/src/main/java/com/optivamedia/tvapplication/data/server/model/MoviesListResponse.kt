package com.optivamedia.tvapplication.data.server.model

import com.google.gson.annotations.SerializedName

data class MoviesListResponse(
        @SerializedName("metadata") val metadata : Metadata?,
        @SerializedName("response") val movieResponse : List<MovieResponse>?
)

data class Metadata (
        @SerializedName("responseElementType") val responseElementType : String?,
        @SerializedName("name") val name : String?,
        @SerializedName("value") val value : String?
)


data class MovieResponse (
        @SerializedName("metadata") val metadata : List<Metadata>?,
        @SerializedName("prLevel") val prLevel : Any?,
        @SerializedName("keywords") val keywordswords : String?,
        @SerializedName("year") val year : Any? ,
        @SerializedName("seriesNumberOfEpisodes") val seriesNumberOfEpisodes : String?,
        @SerializedName("tvShowReference") val tvShowReference : TvShowReference?,
        @SerializedName("episodeId") val episodeId : String?,
        @SerializedName("type") val type : String?,
        @SerializedName("assetExternalId") val assetExternalId : String?,
        @SerializedName("contentProvider") val contentProvider : String?,
        @SerializedName("reviews") val reviews : List<String>?,
        @SerializedName("id") val id : Any?,
        @SerializedName("rentalPeriodUnit") val rentalPeriodUnit : String?,
        @SerializedName("isSecured") val isSecured : Boolean?,
        @SerializedName("windowStart") val windowStart : Any?,
        @SerializedName("adsInfo") val adsInfo : String?,
        @SerializedName("seriesName") val seriesName : String?,
        @SerializedName("prName") val prName : String?,
        @SerializedName("rentalPeriod") val rentalPeriod : String?,
        @SerializedName("name") val name : String,
        @SerializedName("broadcastTime") val broadcastTime : Any?,
        @SerializedName("shortName") val shortName : String?,
        @SerializedName("discountId") val discountId : String?,
        @SerializedName("status") val status : Any?,
        @SerializedName("advisories") val advisories : String?,
        @SerializedName("template") val template : String?,
        @SerializedName("attachments") val attachments : List<Attachments>?,
        @SerializedName("chapters") val chapters : List<String>?,
        @SerializedName("externalChannelId") val externalChannelId : String?,
        @SerializedName("reviewerRating") val reviewerRating : String?,
        @SerializedName("flags") val flags : Any?,
        @SerializedName("description") val description : String?,
        @SerializedName("seriesSeason") val seriesSeason : String?,
        @SerializedName("allowedTerminalCategories") val allowedTerminalCategories : List<AllowedTerminalCategories>?,
        @SerializedName("duration") val duration : Any?,
        @SerializedName("genreEntityList") val genreEntityList : List<GenreEntityList>?,
        @SerializedName("responseElementType") val responseElementType : String?,
        @SerializedName("plannedPublishDate") val plannedPublishDate : Any?,
        @SerializedName("simultaneousViewsLimit") val simultaneousViewsLimit : String?,
        @SerializedName("assetId") val assetId : Any?,
        @SerializedName("genres") val genres : List<Any>?,
        @SerializedName("pricingMatrixId") val pricingMatrixId : Any?,
        @SerializedName("definition") val definition : String?,
        @SerializedName("windowEnd") val windowEnd : Any?,
        @SerializedName("encodings") val encodings : List<Encodings>?,
        @SerializedName("externalId") val externalId : String?, // TODO field required to search later the film detail
        @SerializedName("removalDate") val removalDate : Any?,
        @SerializedName("awards") val awards : List<String>?,
        @SerializedName("extrafields") val extrafields : List<Extrafields>?,
        @SerializedName("securityGroups") val securityGroups : List<String>?,
        @SerializedName("contentProviderExternalId") val contentProviderExternalId : String?,
        @SerializedName("categoryId") val categoryId : Any?
)

data class AllowedTerminalCategories (
        @SerializedName("responseElementType") val responseElementType : String?,
        @SerializedName("maxTerminalsOfNonOperator") val maxTerminalsOfNonOperator : Any?,
        @SerializedName("maxTerminals") val maxTerminals : Any?,
        @SerializedName("name") val name : String?,
        @SerializedName("externalId") val externalId : String?
)

data class Encodings (
        @SerializedName("responseElementType") val responseElementType : String?,
        @SerializedName("name") val name : String?
)

data class Extrafields (
        @SerializedName("responseElementType") val responseElementType : String?,
        @SerializedName("name") val name : String?,
        @SerializedName("value") val value : String?
)

data class GenreEntityList (
        @SerializedName("responseElementType") val responseElementType : String?,
        @SerializedName("parentName") val parentName : String?,
        @SerializedName("name") val name : String?,
        @SerializedName("externalId") val externalId : String?,
        @SerializedName("id") val id : Any?
)

class TvShowReference{}

data class Attachments (
        @SerializedName("responseElementType") val responseElementType : String?,
        @SerializedName("assetId") val assetId : String?,
        @SerializedName("name") val name : String?, // type of the image, for the list i will show the one that is named "NAME_APP_SHOW1"
        @SerializedName("assetName") val assetName : String?,
        @SerializedName("value") val value : String? // image link
) {
    companion object{
        const val NAME_VERTICAL = "VERTICAL"
        const val NAME_APP_SHOW1 = "APP_SLSHOW_1"
        const val BASE_URL = "https://smarttv.orangetv.orange.es/stv/api/rtv/v1/images/"
    }
}