package com.optivamedia.tvapplication.data.server

import com.optivamedia.tvapplication.data.server.model.MovieDetailResponse
import com.optivamedia.tvapplication.data.server.model.MovieRecommendationsResponse
import com.optivamedia.tvapplication.data.server.model.MoviesListResponse
import com.optivamedia.tvapplication.data.source.RemoteDataSource
import retrofit2.Response
import javax.inject.Inject

class TvApplicationRemoteDataSource @Inject constructor(private val tvApplicationService: TvApplicationService,
                                                        private val recommendationService: RecommendationService) : RemoteDataSource {

    override suspend fun getMovies(client: String, statuses: String, definitions: String, external_category_id: String, filter_empty_categories: Boolean): Response<MoviesListResponse> =
            tvApplicationService.getMovies(client, statuses, definitions, external_category_id, filter_empty_categories)

    override suspend fun getMovieDetail(client: String, externalId: String?): Response<MovieDetailResponse> =
            tvApplicationService.getMovieDetail(client, externalId)

    override suspend fun getMovieRecommendations(client: String, type: String, subscription: Boolean, filter_viewed_content: Boolean, max_results: Int,
                                                 blend: String, max_pr_level: Int, quality: String, services: String, params: String): Response<MovieRecommendationsResponse> =
            recommendationService.getMovieRecommendations(client, type, subscription, filter_viewed_content, max_results, blend, max_pr_level, quality, services, params)


}