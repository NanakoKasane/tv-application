package com.optivamedia.tvapplication.data.server

import com.optivamedia.tvapplication.data.server.model.MovieDetailResponse
import com.optivamedia.tvapplication.data.server.model.MoviesListResponse
import retrofit2.Response
import retrofit2.http.*

interface TvApplicationService {

//    @Headers("Accept: */*")
    @GET("GetUnifiedList")
    suspend fun getMovies(@Query("client") client: String,
                          @Query("statuses") statuses: String,
                          @Query("definitions") definitions: String,
                          @Query("external_category_id") external_category_id: String,
                          @Query("filter_empty_categories") filter_empty_categories: Boolean)
            : Response<MoviesListResponse>


    @POST("GetVideo")
    suspend fun getMovieDetail(@Query("client") client: String,
                          @Query("external_id") external_id: String?)
            : Response<MovieDetailResponse>




}