package com.optivamedia.tvapplication.data.extension

fun Any?.toInteger() : Int?{
    if (this is Double)
        return this.toInt()
    if (this is String)
        return this.toInt()
    return null
}