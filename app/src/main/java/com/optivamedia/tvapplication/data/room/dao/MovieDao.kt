package com.optivamedia.tvapplication.data.room.dao

import androidx.room.*
import com.optivamedia.tvapplication.data.room.entity.Movie

@Dao
interface MovieDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovie(movie : Movie?) : Long

    @Query("SELECT * FROM movie WHERE favorite = 1") // 0 is false and 1 is true
    suspend fun getFavoritesMovies() : List<Movie>

    @Query("SELECT * FROM movie")
    suspend fun getAllMovies() : List<Movie>


}