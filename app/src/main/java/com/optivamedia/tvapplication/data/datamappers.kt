package com.optivamedia.tvapplication.data

import com.optivamedia.tvapplication.data.extension.toInteger
import com.optivamedia.tvapplication.data.server.model.Attachments
import com.optivamedia.tvapplication.data.server.model.MovieResponse
import com.optivamedia.tvapplication.data.server.model.RecommendationsResponse
import com.optivamedia.tvapplication.domain.Movie as MovieDomain
import com.optivamedia.tvapplication.data.room.entity.Movie as MovieEntity


fun MovieResponse.toDomainMovie() : MovieDomain =
    MovieDomain(id.toString(), year.toInteger(), name, Attachments.BASE_URL + attachments?.find { it.name == Attachments.NAME_APP_SHOW1 }?.value,
            externalId, duration.toString(), type, keywordswords, description)

fun RecommendationsResponse.toDomainMovie() : MovieDomain =
        MovieDomain(id.toString(), null, name, Attachments.BASE_URL + images?.find { it.name == Attachments.NAME_APP_SHOW1 }?.value,
                 "${externalContentId}_PAGE_HD", null, type, null, null)

fun MovieDomain.toEntity() = MovieEntity(id ?:"default", year, name, image, externalId, duration, type, keywordswords, description, favorite)

fun MovieEntity.toDomain() = MovieDomain(id, year, name, image, externalId, duration, type, keywordswords, description, favorite)