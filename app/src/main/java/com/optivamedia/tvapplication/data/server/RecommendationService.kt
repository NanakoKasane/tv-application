package com.optivamedia.tvapplication.data.server

import com.optivamedia.tvapplication.data.server.model.MovieRecommendationsResponse
import com.optivamedia.tvapplication.data.server.model.MoviesListResponse
import retrofit2.Response
import retrofit2.http.POST
import retrofit2.http.Query

interface RecommendationService {

    @POST("GetVideoRecommendationList")
    suspend fun getMovieRecommendations(@Query("client") client: String,
                          @Query("type") type: String,
                          @Query("subscription") subscription: Boolean,
                          @Query("filter_viewed_content") filter_viewed_content: Boolean,
                          @Query("max_results") max_results: Int,
                          @Query("blend") blend: String,
                          @Query("max_pr_level") max_pr_level: Int,
                          @Query("quality") quality: String,
                          @Query("services") services: String,
                          @Query("params") params: String
    ): Response<MovieRecommendationsResponse>

}
