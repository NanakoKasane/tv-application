package com.optivamedia.tvapplication.data.room

import android.content.Context
import androidx.room.*
import com.optivamedia.tvapplication.data.room.dao.MovieDao
import com.optivamedia.tvapplication.data.room.entity.Movie


//@Singleton
@Fts4
@Database(
        entities = [Movie::class],
        version = 2,
        exportSchema = false)
abstract class TvApplicationDatabase : RoomDatabase() {
    companion object {
        fun build(context: Context) =
                try {
                    Room.databaseBuilder(
                            context,
                            TvApplicationDatabase::class.java,
                            "tvapplication_database"
                    ).build()
                } catch (exception: Exception) {
                    val deleteDatabase = context.deleteDatabase("tvapplication_database")
                    reBuildDatabase(context)
                }

        private fun reBuildDatabase(context: Context) =
                Room.databaseBuilder(
                        context,
                        TvApplicationDatabase::class.java,
                        "tvapplication_database"
                ).fallbackToDestructiveMigrationOnDowngrade().build()
    }

    abstract fun movieDao(): MovieDao

}