package com.optivamedia.tvapplication.data.server.model

import com.google.gson.annotations.SerializedName

data class MovieRecommendationsResponse(
        @SerializedName("metadata") val metadata : RecommendationMetadata?,
        @SerializedName("response") val response : List<RecommendationsResponse>?
)

data class RecommendationMetadata(
        @SerializedName("request") val request : String?,
        @SerializedName("fullLength") val fullLength : Any?,
        @SerializedName("timestamp") val timestamp : Any?
)

data class RecommendationsResponse(
        @SerializedName("ratersCount") val ratersCount : Any?,
        @SerializedName("images") val images : List<Images>?,
        @SerializedName("prLevel") val prLevel : Any?,
        @SerializedName("availabilities") val availabilities : List<Availabilities>?,
        @SerializedName("prName") val prName : String?,
        @SerializedName("rating") val rating : Double?,
        @SerializedName("type") val type : String?,
        @SerializedName("ContentProperties") val contentProperties : List<String>?,
        @SerializedName("recommendationReasons") val recommendationReasons : List<String>?,
        @SerializedName("genres") val genres : List<RecommendationGenres>?,
        @SerializedName("name") val name : String?,
        @SerializedName("externalContentId") var externalContentId : String?,
        @SerializedName("id") val id : Any?,
        @SerializedName("contentType") val contentType : String?
)

data class Images (
        @SerializedName("name") val name : String?,
        @SerializedName("value") val value : String?
)


data class RecommendationGenres (
        @SerializedName("name") val name : String?,
        @SerializedName("externalId") val externalId : String?,
        @SerializedName("id") val id : String?
)


data class Availabilities (
        @SerializedName("images") val images : List<Images>?,
        @SerializedName("AvailabilityProperties") val availabilityProperties : List<String>?,
        @SerializedName("videoId") val videoId : String?,
        @SerializedName("startTime") val startTime : Any?,
        @SerializedName("endTime") val endTime : Any?,
        @SerializedName("categories") val categories : List<Categories>?,
        @SerializedName("serviceId") val serviceId : String?
)


data class Categories (
        @SerializedName("categoryName") val categoryName : String?,
        @SerializedName("categoryId") val categoryId : String?
)
