package com.optivamedia.tvapplication.data.room


import com.optivamedia.tvapplication.data.room.entity.Movie
import com.optivamedia.tvapplication.data.source.RoomDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

class TvApplicationRoomDataSource @Inject constructor(db: TvApplicationDatabase) : RoomDataSource {
    private val movieDao = db.movieDao()


    override suspend fun insertMovie(movie: Movie?) = withContext(Dispatchers.IO){
        movieDao.insertMovie(movie)
    }

    override suspend fun getFavoritesMovies(): List<Movie> = withContext(Dispatchers.IO){
        movieDao.getFavoritesMovies()
    }

    override suspend fun getAllMovies(): List<Movie> = withContext(Dispatchers.IO){
        movieDao.getAllMovies()
    }

}