package com.optivamedia.tvapplication.data.repository

import com.optivamedia.tvapplication.data.exception.Failure
import com.optivamedia.tvapplication.data.functional.Either
import com.optivamedia.tvapplication.data.source.PreferencesDataSource
import com.optivamedia.tvapplication.data.source.RemoteDataSource
import com.optivamedia.tvapplication.data.source.RoomDataSource
import com.optivamedia.tvapplication.data.toDomain
import com.optivamedia.tvapplication.data.toDomainMovie
import com.optivamedia.tvapplication.data.toEntity
import com.optivamedia.tvapplication.domain.Movie
import java.lang.Exception
import java.net.ConnectException
import javax.inject.Inject

class MoviesRepository @Inject constructor(private val remoteDataSource: RemoteDataSource,
                                           private val preferencesDataSource: PreferencesDataSource,
                                           private val roomDataSource: RoomDataSource){

    companion object{
        const val client = "json"
        const val statuses = "published"
        const val definitions = "SD;HD;4K"
        const val external_category_id = "SED_3880"
        const val type = "all"
        const val max_results = 10
        const val blend = "ar_od_blend_2424video"
        const val max_pr_level = 8
        const val quality = "SD,HD"
        const val services = "2424VIDEO"
    }

    suspend fun getMovies(): Either<Failure, List<Movie>> = try {
        val response = remoteDataSource.getMovies(client, statuses, definitions, external_category_id, true )
        when (response.isSuccessful) {
            true -> response.body()?.let {
                if (it.movieResponse != null ){
                    val responseList = it.movieResponse.map { it.toDomainMovie() }

                    // get the favorites list from local database:
                    val responseFavoritesList = roomDataSource.getFavoritesMovies().map { it.toDomain() }

                    // update in the response list when a movie is favorite
                    responseList.forEach { movie ->
                        val movieFoundInFavorites = responseFavoritesList.find { it.id == movie.id }
                        if (movieFoundInFavorites != null)
                            movie.favorite = movieFoundInFavorites.favorite
                    }
                    Either.Right(responseList)
                } else
                    Either.Left(Failure.ServerError)
            }
                    ?: Either.Left(Failure.ServerError)
            false -> Either.Left(Failure.ServerError)
        }
    } catch (exception: IllegalArgumentException) {
        Either.Left(Failure.IllegalArgumentFailure())
    } catch (exception: ConnectException) {
        Either.Left(Failure.ServerError)
    } catch (exception : Exception){
        Either.Left(Failure.ServerError)
    }


    suspend fun getMovieDetail(externalId : String?): Either<Failure, Movie> = try {
        val response = remoteDataSource.getMovieDetail(client, externalId)
        when (response.isSuccessful) {
            true -> response.body()?.let {
                if (it.response != null ){
                    Either.Right(it.response.toDomainMovie())
                } else
                    Either.Left(Failure.ServerError)
            }
                    ?: Either.Left(Failure.ServerError)
            false -> Either.Left(Failure.ServerError)
        }
    } catch (exception: IllegalArgumentException) {
        Either.Left(Failure.IllegalArgumentFailure())
    } catch (exception: ConnectException) {
        Either.Left(Failure.ServerError)
    } catch (exception : Exception){
        Either.Left(Failure.ServerError)
    }


    suspend fun saveMovie(movie : Movie?) : Either<Failure, Long> = try {
        val response = roomDataSource.insertMovie(movie?.toEntity())
        Either.Right(response)
    }  catch (exception : Exception){
        Either.Left(Failure.ServerError)
    }

    suspend fun getFavoritesMovies() : Either<Failure, List<Movie>> = try {
        val response = roomDataSource.getFavoritesMovies()
        Either.Right(response.map { it.toDomain() })
    }  catch (exception : Exception){
        Either.Left(Failure.ServerError)
    }


    suspend fun getMovieRecommendations(externalId : String?): Either<Failure, List<Movie>> = try {
        val response = remoteDataSource.getMovieRecommendations(client, type, false, true,
                max_results, blend, max_pr_level, quality, services, "external_content_id:${externalId?.substringBefore("_PAGE_HD")}" )
        when (response.isSuccessful) {
            true -> response.body()?.let {
                if (it.response != null ){
                    Either.Right(it.response.map { it.toDomainMovie() })
                } else
                    Either.Left(Failure.ServerError)
            }
                    ?: Either.Left(Failure.ServerError)
            false -> Either.Left(Failure.ServerError)
        }
    } catch (exception: IllegalArgumentException) {
        Either.Left(Failure.IllegalArgumentFailure())
    } catch (exception: ConnectException) {
        Either.Left(Failure.ServerError)
    } catch (exception : Exception){
        Either.Left(Failure.ServerError)
    }


}