package com.optivamedia.tvapplication.data.server.model

import com.google.gson.annotations.SerializedName

data class MovieDetailResponse(
        @SerializedName("metadata") val metadata : Metadata?,
        @SerializedName("response") val response : MovieResponse?
)