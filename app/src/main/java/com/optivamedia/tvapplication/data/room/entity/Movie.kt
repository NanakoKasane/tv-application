package com.optivamedia.tvapplication.data.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import javax.annotation.Nonnull


@Entity(tableName = "movie")
data class Movie(
        @Nonnull @PrimaryKey val id : String,
        val year : Int?,
        val name : String?,
        val image : String?,
        val externalId : String?,
        val duration : String?,
        val type : String?,
        val keywordswords : String?,
        val description : String?,
        var favorite : Boolean = false
)