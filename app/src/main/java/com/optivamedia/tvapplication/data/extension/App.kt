package com.optivamedia.tvapplication.data.extension

import com.facebook.stetho.Stetho
import com.optivamedia.tvapplication.App
import com.optivamedia.tvapplication.BuildConfig
import timber.log.Timber

fun App.configure() {
    if (BuildConfig.DEBUG){
        Timber.plant(Timber.DebugTree())
    }
    Timber.d("debug")
    Stetho.initializeWithDefaults(this)
}