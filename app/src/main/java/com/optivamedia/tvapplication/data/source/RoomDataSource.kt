package com.optivamedia.tvapplication.data.source

import com.optivamedia.tvapplication.data.room.entity.Movie


interface RoomDataSource {
    suspend fun insertMovie(movie : Movie?) : Long
    suspend fun getFavoritesMovies() : List<Movie>
    suspend fun getAllMovies() : List<Movie>

}