package com.optivamedia.tvapplication.data.source

import com.optivamedia.tvapplication.data.server.model.MovieDetailResponse
import com.optivamedia.tvapplication.data.server.model.MovieRecommendationsResponse
import com.optivamedia.tvapplication.data.server.model.MoviesListResponse
import retrofit2.Response

interface RemoteDataSource {
    suspend fun getMovies(client : String, statuses : String, definitions : String,
              external_category_id : String, filter_empty_categories : Boolean): Response<MoviesListResponse>

    suspend fun getMovieDetail(client : String, externalId : String?) : Response<MovieDetailResponse>

    suspend fun getMovieRecommendations(client: String, type: String, subscription: Boolean, filter_viewed_content: Boolean,
                                        max_results: Int, blend: String, max_pr_level: Int, quality: String,
                                        services: String, params: String):  Response<MovieRecommendationsResponse>
}