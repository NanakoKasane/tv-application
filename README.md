
**TV Application**

Se trata de una aplicación de prueba para administrar películas y marcarlas como favoritas.

Tiempo de realización: Aproximadamente 12 horas.

<br /> 


Arquitectura: **MVVM**

Uso de Data Binding

<br /> 


**Librerías** usadas:
- Retrofit: Para las peticiones API
- Corrutinas: Para la programación asíncrona
- Glide: Gestión de imágenes
- Dagger: Inyección de dependencias
- Timber: Para los logs
- Stetho: Para debug, por ejemplo permite ver con chrome el contenido de la base de datos sqlite
- Room: Base de datos local sqlite
- Junit: Tests

<br /> 

<br /> 


**Pantallas de la App**

<br /> 



- **Listado de películas**

En este listado podemos ver las películas disponibles y añadirlas o eliminarlas de favoritos haciendo click en el icono de estrella.

<img src="https://gitlab.com/NanakoKasane/tv-application/-/raw/develop/app/src/main/res/raw/film_list.jpg" height="500">


<br /> 

<br /> 



- **Búsqueda**

Se puede filtrar por el nombre de dicha película.

<img src="https://gitlab.com/NanakoKasane/tv-application/-/raw/develop/app/src/main/res/raw/search.jpg" height="500">


<br /> 

<br /> 





- **Detalle de la película** 

Haciendo click en una película, nos dirige a su detalle. Aquí podremos ver más datos sobre ella como la sinopsis, la imagen en grande y una recomendación de películas similares. También se puede añadir de favoritos o eliminar de favoritos desde esta pantalla.

<img src="https://gitlab.com/NanakoKasane/tv-application/-/raw/develop/app/src/main/res/raw/film_detail.jpg" height="500">



<br /> 

<br /> 





- **Recomendaciones**

En el detalle de una película vemos las recomendaciones de películas similares a ella. Al hacer click en las recomendaciones, nos lleva su detalle, donde a su vez podremos marcar esta película como favorita o ver a su vez sus recomendaciones.

Para volver atrás, se puede en cualquiera de estas pantallas usando el botón nativo de Android.

<img src="https://gitlab.com/NanakoKasane/tv-application/-/raw/develop/app/src/main/res/raw/film_detail_recommendation.jpg" height="500">


<br /> 

<br /> 




- **Listado de favoritos**

En este listado vemos solo las películas marcadas como favoritas. También permite eliminarlas de favoritos haciendo click en la estrella, y en este caso desaparecerá de la lista (al no ser ya favorita).

<img src="https://gitlab.com/NanakoKasane/tv-application/-/raw/develop/app/src/main/res/raw/favorites_list.jpg" height="500">


